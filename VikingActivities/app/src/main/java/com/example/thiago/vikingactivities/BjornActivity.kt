package com.example.thiago.vikingactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_bjorn.*

class BjornActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bjorn)

        val intent = intent
        val bundle = intent.getBundleExtra("stringBundle")
        val umaString = bundle.getString("str")

        if (!umaString.equals("Main")){
            tvMensagem.setText("Você chegou neste personagem através do personagem " + umaString)
        }

        btAthelstan.setOnClickListener { mostraActivity("1") }
        btFloki.setOnClickListener { mostraActivity("2") }
        btLagertha.setOnClickListener { mostraActivity("3") }
        btRagnar.setOnClickListener { mostraActivity("4") }
    }

    fun mostraActivity(msg: String){

        val bundle = Bundle()
        bundle.putString("str", "Bjorn")

        if (msg == "1"){
            val intent = Intent(this, AthelstanActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "2"){
            val intent = Intent(this, FlokiActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "3"){
            val intent = Intent(this, LagerthaActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "4"){
            val intent = Intent(this, RagnarActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
    }

}
