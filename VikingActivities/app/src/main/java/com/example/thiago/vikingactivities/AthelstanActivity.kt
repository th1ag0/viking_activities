package com.example.thiago.vikingactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_athelstan.*

class AthelstanActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_athelstan)
        val intent = intent
        val bundle = intent.getBundleExtra("stringBundle")
        val umaString = bundle.getString("str")

        if (!umaString.equals("Main")){
            tvMensagem.setText("Você chegou neste personagem através do personagem " + umaString)
        }

        btBjorn.setOnClickListener { mostraActivity("1") }
        btRagnar.setOnClickListener { mostraActivity("2") }

    }
    private fun mostraActivity(msg: String) {
        val bundle = Bundle()
        bundle.putString("str", "Athelstan")

        if (msg == "1"){
            val intent = Intent(this, BjornActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "2"){
            val intent = Intent(this, RagnarActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
    }

}
