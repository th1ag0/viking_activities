package com.example.thiago.vikingactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import kotlinx.android.synthetic.main.activity_ragnar.*

class RagnarActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ragnar)


        val intent = intent
        val bundle = intent.getBundleExtra("stringBundle")
        val umaString = bundle.getString("str")


        if (!umaString.equals("Main")){
            tvMensagem.setText("Você chegou neste personagem através do personagem " + umaString)
        }
        btLagertha.setOnClickListener { mostraActivity("1") }
        btFloki.setOnClickListener { mostraActivity("2") }
        btBjorn.setOnClickListener { mostraActivity("3") }

    }
    fun mostraActivity(msg: String){

        val bundle = Bundle()
        bundle.putString("str", "Ragnar Lothbrok")

        if (msg == "1"){
            val intent = Intent(this, LagerthaActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "2"){
            val intent = Intent(this, FlokiActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "3"){
            val intent = Intent(this, BjornActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }

    }
}

