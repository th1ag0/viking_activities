package com.example.thiago.vikingactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Log.d("MENSAGEM:", "Create")
        btRagnar.setOnClickListener { mostraActivity("1") }
        btLagertha.setOnClickListener { mostraActivity("2") }
        btRollo.setOnClickListener { mostraActivity("3") }
        btFloki.setOnClickListener { mostraActivity("4") }
        btAthelstan.setOnClickListener { mostraActivity("5") }
        btBjorn.setOnClickListener { mostraActivity("6") }
    }
    fun mostraActivity(msg: String){

        val bundle = Bundle()
        bundle.putString("str", "Main")

        if (msg == "1"){
            val intent = Intent(this, RagnarActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "2"){
            val intent = Intent(this, LagerthaActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "3"){
            val intent = Intent(this, RolloActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "4"){
            val intent = Intent(this, FlokiActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "5"){
            val intent = Intent(this, AthelstanActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
        else if (msg == "6"){
            val intent = Intent(this, BjornActivity::class.java)
            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }
    }

    override fun onResume() {
       super.onResume()
        Log.d("MENSAGEM:", "Resume")
    }

    override fun onStop() {
        super.onStop()
        Log.d("MENSAGEM:", "Stop")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MENSAGEM:", "Pause")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MENSAGEM:", "Destroy")
    }
}
