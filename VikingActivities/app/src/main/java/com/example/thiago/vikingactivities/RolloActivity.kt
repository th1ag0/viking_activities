package com.example.thiago.vikingactivities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_rollo.*

class RolloActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rollo)

        val intent = intent
        val bundle = intent.getBundleExtra("stringBundle")
        val umaString = bundle.getString("str")
        if (!umaString.equals("Main")){
            tvMensagem.setText("Você chegou neste personagem através do personagem " + umaString)
        }

        btBjorn.setOnClickListener {
            var bundle = Bundle()
            var intent = Intent(this, BjornActivity::class.java)
            bundle.putString("str", "Rollo")

            intent.putExtra("stringBundle", bundle)
            startActivity(intent)
        }

    }

}
